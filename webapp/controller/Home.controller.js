sap.ui.define([
	"com/soa/Quality_Notification/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (BaseController, JSONModel, Controller, Filter, FilterOperator) {
	"use strict";

	return BaseController.extend("com.soa.Quality_Notification.controller.Home", {
		onInit: function () {

		},

		onAfterRendering: function () {
			this.getView().byId("productInput").setValue("");

			// var that = this;
			// this.getView().byId("productInput").onsapenter = function () {
			// 	that.getView().byId("productInput").getSuggestionRows()[0].getBindingContext("PurchaseOrders").getObject().PurchaseOrder;
			// }.bind(this);
		},

		//Routing
		onSuggestionItemSelected: function (oEvent) {
			var oContext, oPurchaseOrder;

			oContext = oEvent.getParameter("selectedRow").getBindingContext("PurchaseOrders");
			oPurchaseOrder = oContext.getObject();

			this.getOwnerComponent().getRouter().navTo("WizardView", {
				PurchaseOrder: oPurchaseOrder.PurchaseOrder
			}, false);
		},

		onSuggest: function (evt) {
			if (evt.getSource().getValue().length < 4) {
				return;
			}

			var sValue = evt.getParameter("suggestValue");
			var oFilter = new Filter(
				"PurchaseOrder",
				FilterOperator.Contains, sValue
			);

			evt.getSource().getBinding("suggestionRows").filter([oFilter]);

		}
	});
});
sap.ui.define([
	"com/soa/Quality_Notification/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox"
], function (BaseController, JSONModel, Controller, MessageBox) {
	"use strict";

	return BaseController.extend("com.soa.Quality_Notification.controller.WizardView", {

		onInit: function () {
			//Routing
			var oRouter = this.getRouter();
			oRouter.getRoute("WizardView").attachPatternMatched(this._onObjectMatched, this);
			this._wizard = this.byId("wizard");
		},

		//Routing method
		_onObjectMatched: function (oEvent) {
			var args = oEvent.getParameter("arguments");
			var PurchaseOrder = args.PurchaseOrder;

			this.getModel("PurchaseOrders").metadataLoaded().then(function () {
				var sObjectPath = this.getModel("PurchaseOrders").createKey("/Z_C_PurchaseOrder", {
					PurchaseOrder: PurchaseOrder
				});
				this.getView().bindElement({
					path: sObjectPath,
					model: "PurchaseOrders"
				});
			}.bind(this));
		},

		//Routing method
		navback: function () {
			var sPreviousHash;
			if(this.getView().byId("table0").getSelectedItem()){
			this.getView().byId("table0").getSelectedItem().getCells()[4].setValue(0);
			}
this.byId("upload").removeAllIncompleteItems();
this.byId("stepinput0").setValue(0);
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("TargetHome", {}, true /*no history*/ );
			}

		},

		onAfterRendering: function () {
			//Refrshing the WizardView
			if (this.getModel()) {
				this.getView().getModel().destroy();
			}

			var oWizardModel = new JSONModel({
				textvalue: "",
				selectedKey: 0,
				step1: false,
				quantitytxt: this.getResourceBundle().getText("qmapp.qmview.notificationdetails.selectquantitytxt", [0]),
				stepinputval: 0,
				placeholder: this.getResourceBundle().getText("qmapp.qmview.notificationdetails.selectdefecttype.placeholder")
			});

			this.getView().setModel(oWizardModel);

			this.getView().byId("segmentedbtns").setSelectedButton("none");
			this.getView().byId("table0").removeSelections();
			this._wizard.discardProgress(this._wizard.getSteps()[0]);

			this.getView().byId("stepinput0").attachBrowserEvent("click", function (e) {

				this.getParent().setSelected(true);
				this.setEditable(true);
				// this.getParent().getTable().fireSelectionChange(this.getParent().getTable().getSelectedItem());
			});
		},

		onSelectOrderType: function (oEvent) {
			//Selecting Order type triggers next step in the wizard
			this.getView().byId("selectorder").setValidated(true);
			this._wizard.nextStep();

		},
		
beforeItemAdded: function(oItem){
	if (oItem.getSource().getList().getItems().length >= 1){
		oItem.preventDefault();
		return;
	}
	console.log(oItem);
},



		onSelectionChange: function (oEvent) {
			//Validates wizard step2
			var iQuantity, sLabel;

			iQuantity = oEvent.getParameter("listItem").getBindingContext("PurchaseOrders").getObject("PurchaseOrderQuantity");
			sLabel = this.getResourceBundle().getText("qmapp.qmview.notificationdetails.selectquantitytxt", [iQuantity]);
			this.getModel().setProperty("/quantitytxt", sLabel);
			this.getModel().setProperty("/maxmaterialquant", parseFloat(iQuantity));
			var q = oEvent.getParameter("listItem").getBindingContextPath();
			this.getView().byId("header1").bindElement({
				path: q,
				model: "PurchaseOrders"
			});
			var item = oEvent.getParameter("listItem");

			for (var i = 0; i < oEvent.getSource().getItems().length; i++)
				oEvent.getSource().getItems()[i].getCells()[4].setEditable(false);

			//set the selected row's input field to editable.
			item.getCells()[4].setEditable(true);
			item.getCells()[4].$().find('input').focus();
			oEvent.getSource().getColumns()[4].setVisible(true);
			oEvent.getSource().getSelectedItem().getCells()[4].setVisible(true);

		},

		onStepInputChange: function (oEvent) {
	var sMax = oEvent.getSource().getMax(),
		sMin = oEvent.getSource().getMin();
	
	if(oEvent.getSource().getValue() > sMax){
		oEvent.getSource().setValue(sMax);
	}
	if(oEvent.getSource().getValue() < sMin){
		oEvent.getSource().setValue(sMin);
	}
			this.getView().byId("orderitemtable").setValidated(true);
		},
		
		sendNotification: function(sAttachment){
				var oPurchaseOrder = this.getView().getElementBinding("PurchaseOrders").getBoundContext().getObject(), 
			oSelectedItem	 = this.getView().byId("table0").getSelectedItem().getBindingContext("PurchaseOrders").getObject(), 
				oNotificationModel = this.getModel("NotificationSet");
			sap.ui.core.BusyIndicator.show();
	
			var oEntry = {
				PurchaseOrderId: oPurchaseOrder.PurchaseOrder,
				PurchaseOrderItem: oSelectedItem.PurchaseOrderPosition,
				ComplaintText: this.getModel().getProperty("/textvalue"),
				CodeGroup: "ZQMAPP",
				ProblemCode:this.getView().byId("combobox0").getSelectedKey(),
				ComplaintQuant: this.getView().byId("table0").getSelectedItem().getCells()[4].getValue().toString(),
				Attachment: sAttachment
			};

			oNotificationModel.create("/NotificationQMSet", oEntry

				// CodeGroup: this.getView().byId("combobox0").getSelectedKey()
				// ComplaintQuant: this.getView().getModel().getProperty("/stepinput")
			, {
				success: function (oRes) {
					MessageBox.success("Notification " + oRes.NotificationId + " succesfully created", {
						onClose: function (sAction) {
							this.navback();
						}.bind(this)
					});
					sap.ui.core.BusyIndicator.hide();
				}.bind(this),
				error: function (oError) {
					MessageBox.error("An Error occurred " + oError.message, {
						onClose: function (sAction) {
							this.navback();
						}.bind(this)
					});
					sap.ui.core.BusyIndicator.hide();
				}.bind(this)
			});
		
		},

		onComplete: function (oEvent) {
			// //Submits the notification

		if(this.byId("upload").getIncompleteItems().length > 0){
			var photo = this.byId("upload").getIncompleteItems()[0].getFileObject();
		
		this.byId("upload").getIncompleteItems()[0].setProgress(100);
			
var reader = new FileReader();

        reader.onload = function(readerEvt) {
            var binaryString = readerEvt.target.result;
          var  sAttachment = btoa(binaryString);
          this.sendNotification(sAttachment);
        }.bind(this);
reader.readAsBinaryString(photo);
		} else{
			this.sendNotification("");
		}	
		}
	});

});
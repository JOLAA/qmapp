/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"com/soa/Quality_Notification/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});